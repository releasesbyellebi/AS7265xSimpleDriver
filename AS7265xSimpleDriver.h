/*
AS7265xSimpleDriver - driver library for UART-based AS7265x spectrophotometer boards.

Created by Lorenzo Bini, Universita' degli Studi di Milano, 5 December 2022
Spectrophotometer weights by Andrea Perugini, Universita' degli Studi di Milano

Licensed under GNU GPL 2.0

Tested on ESP32

This is a library for basic interaction with AS7265-based spectrophotometer boards that use UART communication (AKA Serial).

Instruction Manual:

0. You must initialize the serial port yourself even after passing it to the AS7265X object, using Serial.begin()...
1. Run initialize at least once before doing anything else
2. Use ledsOn and ledsOff to turn the onboard driver LEDs on and off
3. Use rawRead to perform a spectrophotometric capture. You must provide a float array of length numData (18)
as an argument, which will be filled with the results of the capture, ordered by wavelength (from shortest to longest)
4. Use weighedRead to perform a weighed spectrophotometric capture. A default set of weights by
Andrea Perugini is provided. Accuracy of weights not guaranteed
5. You may use setWeights and getWeights to provide your own set of weights or read the ones currently in use
6. You may use getSensorTemp to obtain the temperature of a specific sensor. Sensors are coded as 1, 2, or 3,
which you must provide as input. The temperature of the selected sensor is returned

Error Detection:

This library includes hardware error detection capability. If an error is detected with the spectrometer board,
functions which return booleans will return false instead of true, and temperature readings will return -1.
Be aware that in this case, the result array of spectrophotometric captures by rawRead and weighedRead is undefined.

The static constant numData, equal to 18, is provided for your convenience to properly size arrays.

*/

#ifndef AS7265Xlib

#define AS7265Xlib

#include "Arduino.h"

class AS7265X
	{
	public:
		static const int numData = 18;
		
		AS7265X (Stream& port);
		
		void initialize ();
		bool ledsOn ();
		bool ledsOff ();
		int getSensorTemp (int sensor);
		bool rawRead (float arr [numData]);
		bool weighedRead (float arr [numData]);
		void setWeights (float src [numData]);
		void getWeights (float dest [numData]);
		
	private:
		Stream& _port;
		float _weights [numData] = {32.61, 2.43, 1.36, 5.42, 3.63, 2.8, 3.34, 3.8, 2.07, 10.29, 10.13, 35.21, 34.0, 41.78, 41.78, 41.78, 41.78, 41.78};      // Default weights
		
		bool readOK ();
		void emptyBuffer ();
		void weighValues (float values [numData]);
	};

#endif
