#include <AS7265xSimpleDriver.h>

#define SPECTR_TX_PIN 33
#define SPECTR_RX_PIN 32        // RX and TX pins for Spectrometer serial

String stringArrayFloat (const float* arr, int aSize)     // This takes an array of size aSize and turns it into a semicolon-separated string of values, which is CSV-friendly (second-level)
  {
  String s = String(arr[0]);                              // Start by putting the first number, without semicolons
  for (int i = 1; i < aSize; i++)
    {
    s = s + "," + String(arr[i]);                         // Then put the rest of the numbers, each preceded by a comma
    }
  return s;
  }

AS7265X spectr(Serial1);

void setup ()
  {
  Serial.begin(115200);
  delay(1000);                                  // Wait a moment for serial port
  Serial.println("Starting...");

  Serial1.begin(115200, SERIAL_8N1, SPECTR_RX_PIN, SPECTR_TX_PIN);        // Spectrometer serial using hardware UART on custom pins: 115200 is from the library, 8N1 just works
  spectr.initialize();
  }

// Reminder: true is 1, false is 0

void loop ()
  {
  Serial.println("LEDs on result: " + String(spectr.ledsOn()));
  Serial.println("Temperature values:");
  Serial.println(spectr.getSensorTemp(1));
  Serial.println(spectr.getSensorTemp(2));
  Serial.println(spectr.getSensorTemp(3));
  float theArr [spectr.numData];
  bool r;
  
  r = spectr.rawRead(theArr);
  Serial.println("Raw Read Result:");
  Serial.println(stringArrayFloat(theArr, spectr.numData));
  Serial.println("Read Good?");
  Serial.println(r);

  r = spectr.weighedRead(theArr);
  Serial.println("Weighed Read Result (default weights):");
  Serial.println(stringArrayFloat(theArr, spectr.numData));
  Serial.println("Read Good?");
  Serial.println(r);

  spectr.getWeights(theArr);
  Serial.println("Old Weights:");
  Serial.println(stringArrayFloat(theArr, spectr.numData));

  float w [spectr.numData] = {0.0};
  spectr.setWeights(w);

  spectr.getWeights(theArr);
  Serial.println("New Weights:");
  Serial.println(stringArrayFloat(theArr, spectr.numData));

  r = spectr.weighedRead(theArr);
  Serial.println("New Weighed Read Result:");
  Serial.println(stringArrayFloat(theArr, spectr.numData));
  Serial.println("Read Good?");
  Serial.println(r);
  
  delay(1000);
  Serial.println("LEDs off result: " + String(spectr.ledsOff()));
  delay(1000);

  Serial.println("Spectrometer Demo Finished");
  for (;;)
    {
    delay(1000);
    }
  }
