/*
AS7265xSimpleDriver - driver library for UART-based AS7265x spectrophotometer boards.

Created by Lorenzo Bini, Universita' degli Studi di Milano, 5 December 2022
Spectrophotometer weights by Andrea Perugini, Universita' degli Studi di Milano

Licensed under GNU GPL 2.0

Tested on ESP32

This is a library for basic interaction with AS7265-based spectrophotometer boards that use UART communication (AKA Serial).

Instruction Manual:

0. You must initialize the serial port yourself even after passing it to the AS7265X object, using Serial.begin()...
1. Run initialize at least once before doing anything else
2. Use ledsOn and ledsOff to turn the onboard driver LEDs on and off
3. Use rawRead to perform a spectrophotometric capture. You must provide a float array of length numData (18)
as an argument, which will be filled with the results of the capture, ordered by wavelength (from shortest to longest)
4. Use weighedRead to perform a weighed spectrophotometric capture. A default set of weights by
Andrea Perugini is provided. Accuracy of weights not guaranteed
5. You may use setWeights and getWeights to provide your own set of weights or read the ones currently in use
6. You may use getSensorTemp to obtain the temperature of a specific sensor. Sensors are coded as 1, 2, or 3,
which you must provide as input. The temperature of the selected sensor is returned

Error Detection:

This library includes hardware error detection capability. If an error is detected with the spectrometer board,
functions which return booleans will return false instead of true, and temperature readings will return -1.
Be aware that in this case, the result array of spectrophotometric captures by rawRead and weighedRead is undefined.

The static constant numData, equal to 18, is provided for your convenience to properly size arrays.

*/

#include "Arduino.h"
#include "AS7265xSimpleDriver.h"

// Private

bool AS7265X::readOK ()                // Internal use only: read and consume a single OK from the spectrometer. Returns True if the OK was present, False otherwise
  {
  String r = _port.readStringUntil('\n');   // Note that the terminating character is discarded
  r.trim();                                 // This is in-place and doesn't return anything
  return (r.equals("OK"));
  }

void AS7265X::emptyBuffer ()            // Internal use only: empty the serial buffer
  {
  while (_port.available())
  {
  _port.read();
  }

  return;
  }

void AS7265X::weighValues (float values[numData])
  {
  for (int i = 0; i < numData; i++)
    {
    values[i] = values[i] * _weights[i];
    }
  return;
  }

// Public

AS7265X::AS7265X (Stream& port) : _port(port) 	// Initialization list is necessary because _port can't be reassigned because it is a reference
	{
	return;
	}

void AS7265X::initialize()						// Turn all driver LEDs on. This should be done before capturing data
	{
	delay(2000);
	_port.write("ATFRST\r\n");
	delay(2000);

	emptyBuffer();
      
	return;	
	}

bool AS7265X::ledsOn()
	{
	delay(100);
      
	_port.write("ATLED1=1\r\n");
	delay(100);
	_port.write("ATLED3=1\r\n");
	delay(100);
	_port.write("ATLED5=1\r\n");
      
	delay(100);

	return (readOK() && readOK() && readOK());	// Return whether these all returned true, and thus everything is OK
	}

bool AS7265X::ledsOff()
	{
	delay(100);

	_port.write("ATLED1=0\r\n");
	delay(100);
	_port.write("ATLED3=0\r\n");
	delay(100);
	_port.write("ATLED5=0\r\n");
      
	delay(100);
      
	return (readOK() && readOK() && readOK());	// Return whether these all returned true, and thus everything is OK
	}

int AS7265X::getSensorTemp (int sensor)			// Returns the current temperature of the specified sensor using the ATTEMP command. The sensor may be 1, 2, or 3. If the argument is invalid or an error occurs, -1 is returned.
	{
	delay(100);
	_port.write("ATTEMP\r\n");
	delay(100);
      
	int t1 = _port.parseInt();
	int t2 = _port.parseInt();
	int t3 = _port.parseInt();

	if (!readOK()) return -1;                  // WARNING EARLY RETURN HERE
      
	switch (sensor)
		{
		case 1:
			return t1;
			break;
		case 2:
			return t2;
			break;
		case 3:
			return t3;
			break;
		default:
			return -1;
		}	
	}

bool AS7265X::rawRead (float arr [numData])		// Read band values and order them; arr must be an array of 18 floats. Returns true if read executed correctly, false otherwise. Note that in the latter case, the array values become undefined. Always check the return value!
	{
	delay(100);
	_port.write("ATCDATA\r\n");                  			// Give the sensor some time to do its thing, then read
	delay(500);                                   			// This returns 18 comma-separated floats

	float fArr [numData];                       			// This is the array that will contain the values
  
	String sArr = _port.readString();             			// String for reading the serial data to

	// Parse the string and put the numbers into the float array
	String temp = "";
	int start = 0;
	int end = 0;                                    		// This will become 0 at the start of the cycle
	sArr = sArr + ",";                         				// To enforce string structure and avoid potentially undefined behavior in the parsing cycle
	bool errorFlag = false;

	fArr[0] = sArr.toFloat();                               // The very first float is a special case because it doesn't start with a comma
	for (int i = 1; i < numData; i++)
		{
		start = sArr.indexOf(',', end);
		end = sArr.indexOf(',', start + 1);

		if ((start == -1) || (end == -1)) errorFlag = true;	// If this is raised, it means malformed data was received. An error should be signaled to the user
    
		temp = sArr.substring(start + 1, end);
		fArr[i] = temp.toFloat();
		}

		emptyBuffer();
      
		arr[0] = fArr[12];                          		// Order the data by increasing wavelength
		arr[1] = fArr[13];
		arr[2] = fArr[14];
		arr[3] = fArr[15];
		arr[4] = fArr[16];
		arr[5] = fArr[17];
		arr[6] = fArr[6];
		arr[7] = fArr[7];
		arr[8] = fArr[0];
		arr[9] = fArr[8];
		arr[10] = fArr[1];
		arr[11] = fArr[9];
		arr[12] = fArr[2];
		arr[13] = fArr[3];
		arr[14] = fArr[4];
		arr[15] = fArr[5];
		arr[16] = fArr[10];
		arr[17] = fArr[11];

		return !errorFlag;
	}

bool AS7265X::weighedRead (float arr [numData]) 	// Read weighed band values and order them. Same syntax as rawRead
	{
	bool result;
      
	result = rawRead (arr);
	weighValues(arr);

	return result;
	}

void AS7265X::setWeights (float src [numData])
	{
	for (int i = 0; i < numData; i++)
		{
		_weights[i] = src[i];
		}
      
	return;
	}

void AS7265X::getWeights (float dest [numData])
	{
	for (int i = 0; i < numData; i++)
		{
		dest[i] = _weights[i];
		}

	return;
	}
